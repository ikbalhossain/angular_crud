import { Component } from '@angular/core';
import {Http, Response,Headers} from '@angular/http';
import {Observable} from 'rxjs/Rx';

@Component({
    selector: 'userlist',
    template: `
            <section class="danger">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h2>User List</h2>
                            <hr class="star-light">
                        </div>
                    </div>
                    <div class="row">
                        <ul style="list-style-type:circle">
                             <li *ngFor="let auto of autos">
                                {{ auto.business_type }}
                            </li>
                        </ul>
                    </div>
                </div>
            </section>
				
    		  `
   
})
export class UserlistComponent { 
  public autos: string;
  
    
  constructor(private http: Http) { }
  ngOnInit() {
    this.getAutos();
  }
 
  getAutos() {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.post('http://localhost:8053/EicraCMS30040/Administrator/autos/type/find','',headers)
             .map((res:Response) => res.json())
             .subscribe(
               data => { this.autos = data.data_result},
               err => console.error(err),
               () => console.log(this.autos)
             );
  }
}
