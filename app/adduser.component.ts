import {Injectable, Inject} from '@angular/core';
import { Component } from '@angular/core';
import {Http, Response,Headers,RequestOptions,RequestMethod} from '@angular/http';
import {Observable} from 'rxjs/Rx';


@Component({
    selector: 'add-user',
    template: `
            <section class="danger">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h2>User Add</h2>
                            <hr class="star-light">
                        </div>
                    </div>
                    <div class="row">
                        <form  (ngSubmit)="onSubmit()" #businesstypeForm="ngForm">
                            <fieldset>
                                <div class="form-group">
                                    <label>User Name</label>
                                    <input name="user_name" type="text" class="form-control" placeholder="User Name">
                                </div>
                                <div class="form-group">
                                    <label>User Email</label>
                                    <input name="user_email" type="text" class="form-control" placeholder="User Email">
                                </div>
                                <div class="form-group">
                                    <label>User Country</label>
                                    <input name="user_country" type="text" class="form-control" placeholder="User Country">
                                </div>
                                
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </fieldset>
                        </form>    
                    </div>
                </div>
            </section>
				
    		  `,
              
   
})

@Injectable()
export class AdduserComponent { 
    public data: any = {};
    public result: any;
    public request_url: string  = 'http://localhost:8053/EicraCMS30040/Administrator/autos/type/add';

    constructor(private http:Http) {
        //models[0][id]:
        this.data['user_name'] = 'ikbal';
        this.data['user_email'] = 'ikbal.official@gmail.com';
        //var creds = "username=" + username + "&password=" + password;
        // var user_name:string = 'ikbal';
        // var user_email:string = 'ikbal.official@gmail.com';
        this.data = "posted_data=" + JSON.stringify(this.data);
        //this.data = this.data;
        
        //{group_id:'1',business_type:'hello angular'};
    }

    onSubmit() {        
        
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        headers.append("enctype", "multipart/form-data");
        headers.append("Accept", "application/json, text/javascript, */*;");
        headers.append("X-Requested-With", "XMLHttpRequest");
        
        let options = new RequestOptions({
            method: RequestMethod.Post,
            headers: headers,
            url:this.request_url,
            body: this.data
            
        });


        this.http.post(this.request_url, this.data,options)
            .subscribe(
               data => { this.result = data},
               err => console.error(err),
               () => console.log(this.result)
            );
    }






 


}
