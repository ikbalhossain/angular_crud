
import { provideRouter, RouterConfig } from '@angular/router';
import { UserlistComponent } from './userlist.component';
import { AdduserComponent } from './adduser.component';
import { AppComponent } from './app.component';

const routes: RouterConfig = [
  { path: '', component: AppComponent },
  
  { path: 'user-list', component: UserlistComponent },
  { path: 'user-add', component: AdduserComponent },
];

export const appRouterProviders = [
  provideRouter(routes)
];