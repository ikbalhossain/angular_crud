import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { PageHeaderComponent } from './page-header.component';
import { PortofolioComponent } from './portofolio.component';
import { AboutusComponent } from './aboutus.component';
import { ContactComponent } from './contact.component';
import { FooterComponent } from './footer.component';
import { TopgoComponent } from './topgo.component';
@Component({
    selector: 'angular-app',
    template: ` <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
					<div class="container">
						<div class="navbar-header page-scroll">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
							</button>
							<a class="navbar-brand" href="#page-top">Start Bootstrap</a>
						</div>        
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav navbar-right">
								<li class="hidden">
									<a href="#page-top"></a>
								</li>
								<li class=""><a routerLink="/user-list" routerLinkActive="active">User List</a></li>
								<li class=""><a routerLink="/user-add" routerLinkActive="active">New User</a></li>
								<li class="page-scroll">
									<a href="#about">About</a>
								</li>
								<li class="page-scroll">
									<a href="#contact">Contact</a>
								</li>
							</ul>
						</div>            
					</div>
				</nav>
				<router-outlet></router-outlet>
				<page-header></page-header>
    			<portofolio></portofolio>				
    			<aboutus></aboutus>
    			<contact></contact>
				<footer></footer>
    			<topgo></topgo>
    		  `,
	directives:[PageHeaderComponent,PortofolioComponent,AboutusComponent,ContactComponent,FooterComponent,TopgoComponent,ROUTER_DIRECTIVES]
   
})
export class AppComponent { }
